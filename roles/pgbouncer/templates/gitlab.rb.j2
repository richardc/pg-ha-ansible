# Disable all components except Pgbouncer and Consul agent
bootstrap['enable'] = false
gitaly['enable'] = false
mailroom['enable'] = false
nginx['enable'] = false
redis['enable'] = false
prometheus['enable'] = false
postgresql['enable'] = false
unicorn['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
gitlab_rails['auto_migrate'] = false

pgbouncer['enable'] = true
consul['enable'] = true

# Configure Pgbouncer
pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)

# Configure Consul agent
consul['watchers'] = %w(postgresql)

# START user configuration
# Please set the real values as explained in Required Information section
# Replace CONSUL_PASSWORD_HASH with with a generated md5 value
# Replace PGBOUNCER_PASSWORD_HASH with with a generated md5 value
pgbouncer['users'] = {
  'gitlab-consul': {
    password: '{{ CONSUL_PASSWORD_HASH }}'
  },
  'pgbouncer': {
    password: '{{ PGBOUNCER_PASSWORD_HASH }}'
  }
}
# Replace placeholders:
#
# Y.Y.Y.Y consul1.gitlab.example.com Z.Z.Z.Z
# with real information.
consul['configuration'] = {
  bind_addr: '{{ ansible_eth1.ipv4.address }}',
  retry_join: %w(
  {% for ip in groups['consul'] | map('extract', hostvars, ['ansible_eth1', 'ipv4', 'address']) | list %}
    {{ ip }}
  {% endfor %}
  )
}
#
# END user configuration
