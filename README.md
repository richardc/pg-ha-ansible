# Sandbox for pg-ha

Some ansible roles to automate setup of a PG HA env.

We're using DO droplets, so we do some messy things like log in as root.   This
is baby steps ansible for me, but I'm worse at repeated copy and paste.

# Steps

Create a vault of passwords.  If you don't do this the default passwords from
`group_vars/all/bars` will be used, they're not very secure.

~~~
ansible-vault create group_vars/all/vault
~~~

Create a hosts file from the output of https://gitlab.com/gitlab-com/dev-resources/

~~~
./make_inventory.rb rclamp < output.txt > hosts.rclamp
~~~



Check the hosts can be reached:

~~~
ansible -i hosts.rclamp all -m raw -a 'echo foo'
~~~

Create a vault of passwords

~~~
ansible-vault create group_vars/all/vault
~~~

Run this playbook against those nodes

~~~
ansible-playbook -i hosts.rclamp site.yml --vault-password-file vault.password
~~~

# Useful/explorative things

Check the facts available

~~~
ansible  -i hosts.rclamp pgbouncer -m setup
~~~

You can limit to a node to iterate, but since we grab inventory from groups that
might not be smart:

~~~
ansible-playbook --limit rclamp-consul-a -i hosts.rclamp site.yml
~~~
