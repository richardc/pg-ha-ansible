#!/usr/bin/env ruby
# Makes an ansible inventory from the output of
# https://gitlab.com/gitlab-com/dev-resources/ deploy_develpment job
#
# Usage:  ./make_inventory.rb rclamp < output.txt > hosts.rclamp
user = ARGV.shift
raise "Need a user" unless user

groups = {}
STDIN.each_line do |line|
  next unless line =~ /Public ip (#{user}-.*?) = (.*)/
  host, ip = $1, $2
  host =~ /^#{user}-(.*?)(?:-.*)?$/
  group = $1
  groups[group] ||= {}
  groups[group][host] = ip
end

puts <<-END
[all:vars]
ansible_user=root

END

groups.each do |group, members|
  puts "[#{group}]"
  members.each do |name, address|
    puts "#{name} ansible_host=#{address}"
  end
  puts
end

